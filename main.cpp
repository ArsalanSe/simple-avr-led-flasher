#include <avr/io.h>
#include <util/delay.h>

#define LP PORTB
int speed=100;

void mledltr() {
	char ledltr[9]={0b00000000,0b10000000,0b11000000,0b11100000,0b11110000,0b11111000,0b11111100,0b11111110,0b11111111};
	char ledrtl[9]={0b11111111,0b01111111,0b00111111,0b00011111,0b00001111,0b00000111,0b00000011,0b00000001,0b00000000};

	for(int i=0;i<9;i++){
		LP=ledltr[i];
		_delay_ms(speed);
	}

	for(int i=1;i<9;i++){
		LP=ledrtl[i];
		_delay_ms(speed);
	}
}

void mledrtl() {
	char ledltr[9]={0b00000000,0b10000000,0b11000000,0b11100000,0b11110000,0b11111000,0b11111100,0b11111110,0b11111111};
	char ledrtl[9]={0b11111111,0b01111111,0b00111111,0b00011111,0b00001111,0b00000111,0b00000011,0b00000001,0b00000000};

	for(int i=7;i>=0;i--){
		LP=ledrtl[i];
		_delay_ms(speed);
	}

	for(int i=7;i>=0;i--){
		LP=ledltr[i];
		_delay_ms(speed);
	}
}

void middel(){
	char list[7]={0b10000001,0b11000011,0b11100111,0b11111111,0b11100111,0b11000011,0b10000001};
	char list2[7]={0b00011000,0b00111100,0b01111110,0b11111111,0b01111110,0b00111100,0b00011000};

	for(int i=0;i<7;i++){
		LP=list[i];
		_delay_ms(speed);
	}

	for(int i=0;i<7;i++){
		LP=list2[i];
		_delay_ms(speed);
	}
}

void moledrtl(){
	char list3[29]={0b00000001,0b10000001,0b01000001,0b00100001,0b00010001,0b00001001,0b00000101,0b00000011,
				  0b10000011,0b01000011,0b00100011,0b00010011,0b00001011,0b00000111,
				  0b10000111,0b01000111,0b00100111,0b00010111,0b00001111,
				  0b10001111,0b01001111,0b00101111,0b00011111,
				  0b10011111,0b01011111,0b00111111,
				  0b10111111,0b01111111,
				  0b11111111};

	char list4[29]={0b10000000,0b10000001,0b10000010,0b10000100,0b10001000,0b10010000,0b10100000,0b11000000,
				  0b11000001,0b11000010,0b11000100,0b11001000,0b11010000,0b11100000,
				  0b11100001,0b11100010,0b11100100,0b11101000,0b11110000,
				  0b11110001,0b11110010,0b11110100,0b11111000,
				  0b11111001,0b11111010,0b11111100,
				  0b11111101,0b11111110,
				  0b11111111};

	for(int i=0;i<29;i++){
		LP=list3[i];
		_delay_ms(speed);
	}
	for(int i=0;i<29;i++){
		LP=list4[i];
		_delay_ms(speed);
	}
}

void oledltr() {
	LP=0b00000001;
	_delay_ms(speed);

	for(int i=1;i<8;i++) {
		LP=LP<<1;
		_delay_ms(speed);
	}
}

void oledrtl() {
	//LP=0b10000000;
	//_delay_ms(speed);

	for(int i=1;i<8;i++) {
		LP=LP>>1;
		_delay_ms(speed);
	}
}

void flash() {
	for(int i=1;i<8;i++) {
		LP=0b00000000;
		_delay_ms(speed);
		LP=0b11111111;
		_delay_ms(speed);
	}
	for(int i=1;i<8;i++) {
		LP=0b01010101;
		_delay_ms(speed);
		LP=0b10101010;
		_delay_ms(speed);
	}
}

void black() {
	LP=0b11111111;

	char list[8]={0b11111110,0b11111101,0b11111011,0b11110111,0b11101111,0b11011111,0b10111111,0b01111111};

	for(int i=0;i<8;i++) {
		LP=list[i];
		_delay_ms(speed);
	}

	for(int i=6;i>=0;i--) {
		LP=list[i];
		_delay_ms(speed);
	}
}

int main(void) {

	DDRB=0xFF;

	while(1) {
		mledltr();
		_delay_ms(speed);
		mledrtl();
		_delay_ms(speed);
		middel();
		_delay_ms(speed);
		moledrtl();
		_delay_ms(speed);
		oledltr();
		_delay_ms(speed);
		oledrtl();
		_delay_ms(speed);
		flash();
		_delay_ms(speed);
		black();
	}

	return 1;
}